import "./App.css";
import ExpenseTracker from "./components/ExpenseTracker";
import IncomeTracker from "./components/IncomeTracker";

function App() {
  const expenseDummy = [
    {
      eDate: new Date(2021, 0, 1),
      eInfo: "Start recording your expenses here",
      eAmount: "xxxx",
    },
    {
      eDate: new Date(2020, 0, 1),
      eInfo: "Dummy expense II",
      eAmount: "yyy",
    }
  ];
  const addExpenseHandler = expense => {
    console.log("In App.js");
    console.log(expense)
  }
  
  const incomeDummy = [
    {
      iDate: new Date(2021, 0, 1),
      iInfo: "Start recording your incomes here",
      iAmount: "xxxx",
    },
    {
      iDate: new Date(2019, 0, 1),
      iInfo: "Another income dummy",
      iAmount: "zzzzz",
    },
  ];
  const addIncomeHandler = income => {
    console.log("In App.js");
    console.log(income)
  }
  
 
  return (
    <div id="body">
      <div id="mainBox">
        <h1>Personal Expense Tracker</h1>
        <div id="etMain">
          <ExpenseTracker OnAddExpense={addExpenseHandler}
            eDate={expenseDummy[0].eDate}
            eInfo={expenseDummy[0].eInfo}
            eAmount={expenseDummy[0].eAmount}
          />
          <IncomeTracker OnAddIncome={addIncomeHandler}
            iDate={incomeDummy[0].iDate}
            iInfo={incomeDummy[0].iInfo}
            iAmount={incomeDummy[0].iAmount}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
