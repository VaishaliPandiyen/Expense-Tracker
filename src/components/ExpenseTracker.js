import React, { useState } from "react";
import "./styles/Tracker.css";
import Filter from "./Filter";
import ExpenseDate from "./ExpenseDate";
import NewExpense from "./NewExpense";
import Card from "./Card";

function ExpenseTracker(props) {
  const saveNewExpense = (newExpenseData) => {
    const expenseData = { ...newExpenseData };
    props.OnAddExpense(expenseData);
  };

  const [filteredYear, setFilteredYear] = useState('2022');
  const filterChangeHander = (selectedYear) => {
    setFilteredYear(selectedYear);
  }
  return (
    <div id="expenses">
      <Filter id="expenseFilter" selected={filteredYear} OnChangeFilter={filterChangeHander}/>
      <Card className="eachExpense">
        <ExpenseDate eDate={props.eDate} />
        <div className="info">{props.eInfo}</div>
        <div className="amount">
          {"\u20B9"}
          {props.eAmount}
        </div>
      </Card>
      <NewExpense OnSaveNewExpense={saveNewExpense} />
    </div>
  );
}

export default ExpenseTracker;
