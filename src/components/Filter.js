import "./styles/Filter.css";

export default function Filter(props) {
  const selectYearHandler = (event) => {
    props.OnChangeFilter(event.target.value)
  }
  return (
    <div className="filter">
      <div className="filter-wrapper">
        <label>Filter By Year</label>
        <select onChange={selectYearHandler}>
          <option value="2022">2022</option>
          <option value="2021">2021</option>
          <option value="2020">2020</option>
          <option value="2019">2019</option>
        </select>
      </div>
    </div>
  );
}
