import './styles/ExpIncDate.css'

function incomeDate(props) {
  let iDate = props.iDate.toLocaleString("en-US", { day: "2-digit" });
  let iMonth = props.iDate.toLocaleString("en-US", { month: "short" });
  let iYear = props.iDate.getFullYear();

  return (
    <div className="date">
      <div className="dateLine1">{iDate}</div>
      <div className="dateLine1">{iMonth}</div>
      <div className="dateLine2">{iYear}</div>
    </div>
  );
}

export default incomeDate;
