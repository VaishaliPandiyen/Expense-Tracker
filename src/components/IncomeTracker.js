import React, { useState } from "react";
import "./styles/Tracker.css";
import Filter from "./Filter";
import IncomeDate from "./IncomeDate";
import NewIncome from "./NewIncome";
import Card from "./Card";

function IncomeTracker(props) {
  const saveNewIncome = (newIncomeData) => {
    const incomeData = { ...newIncomeData };
    props.OnAddIncome(incomeData);
  };

  const [filteredYear, setFilteredYear] = useState('2020');
  const filterChangeHander = (selectedYear) => {
    setFilteredYear(selectedYear);
  }

  return (
    <div id="income">
      <Filter id="incomeFilter" selected={filteredYear} OnChangeFilter={filterChangeHander}/>
      <Card className="eachIncome">
        <IncomeDate iDate={props.iDate} />
        <div className="info">{props.iInfo}</div>
        <div className="amount">
          {"\u20B9"}
          {props.iAmount}
        </div>
      </Card>
      <NewIncome OnSaveNewIncome={saveNewIncome} />
    </div>
  );
}

export default IncomeTracker;
