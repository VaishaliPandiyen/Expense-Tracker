import React, { useState } from "react";
import "./styles/New.css";

function NewIncome(props) {
  const [newDate, setNewDate] = useState("");
  const newDateHandler = (event) => {
    setNewDate(event.target.value);
  };

  const [newAmount, setNewAmount] = useState("");
  const newAmountHandler = (event) => {
    setNewAmount(event.target.value);
  };

  const [newTitle, setNewTitle] = useState("");
  const newInfoHandler = (event) => {
    setNewTitle(event.target.value);
  };

  const incomeSubmitHandler = (event) => {
    event.preventDefault();

    const incomeData = {
      iDate: new Date(newDate),
      iAmount: newAmount,
      iTitle: newTitle,
    };

    props.OnSaveNewIncome(incomeData);

    setNewDate("");
    setNewAmount("");
    setNewTitle("");
  };
  return (
    <div>
      <form onSubmit={incomeSubmitHandler} className="newForm">
        <div>
          <div className="newDetails">
            <label for="niDate">Date </label>
            <input
              type="date"
              name="niDate"
              min="2020-01-01"
              max="2022-12-31"
              onChange={newDateHandler}
              value={newDate}
            />
          </div>
          <div className="newDetails">
            <label for="niAmount">Cost </label>
            <input
              type="number"
              name="niAmount"
              min="0.01"
              step="0.01"
              onChange={newAmountHandler}
              value={newAmount}
            />
          </div>
          <div className="newDetails newAddInfo">
            <label for="niInfo">Purpose/Source </label>
            <input
              type="text"
              name="niInfo"
              onChange={newInfoHandler}
              value={newTitle}
            />
          </div>
          <div className="submitDiv">
            <button type="submit" className="submitBtn">
              LOG
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}

export default NewIncome;
