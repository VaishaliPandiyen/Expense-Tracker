import './styles/ExpIncDate.css';

function expenseDate(props) {
  let eDate = props.eDate.toLocaleString("en-US", { day: "2-digit" });
  let eMonth = props.eDate.toLocaleString("en-US", { month: "short" });
  let eYear = props.eDate.getFullYear();

  return (
    <div className="date">
      <div className="dateLine1">{eDate}</div>
      <div className="dateLine1">{eMonth}</div>
      <div className="dateLine2">{eYear}</div>
    </div>
  );
}

export default expenseDate;